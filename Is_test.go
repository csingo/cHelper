package cHelper

import (
	"log"
	"testing"
)

type A struct {
	AAA string `json:"aaa"`
	BB  *B     `json:"bb"`
}

type E struct {
}

type B struct {
	BBB string `json:"bbb"`
	CC  *C     `json:"cc"`
	EE  []*C   `json:"ee"`
}

type C struct {
	CCC string `json:"ccc"`
	DD  []*D   `json:"dd"`
}

type D struct {
	DDD string `json:"ddd"`
	AA  *A     `json:"aa"`
}

var o = map[string]*A{
	"m": {
		AAA: "oa",
		BB: &B{
			BBB: "ob",
			CC: &C{
				CCC: "oc",
				DD: []*D{
					{DDD: "od1"},
					{DDD: "od2"},
				},
			},
			EE: []*C{
				{
					CCC: "oec1",
					DD: []*D{
						{DDD: "od3"},
						{DDD: "od4"},
					},
				},
				{
					CCC: "oec2",
					DD: []*D{
						{DDD: "od5"},
						{DDD: "od6"},
					},
				},
			},
		},
	},
}

func TestIsMap(t *testing.T) {
	// a := []string{"1", "1", "1"}
	log.Printf("%#v", GetFieldValues("m.BB.EE.*.DD.*.DDDV", o))
}
