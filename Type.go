package cHelper

type RandomType uint8

const (
	RandomType_All RandomType = iota
	RandomType_Number
	RandomType_Char
	RandomType_LowerChar
	RandomType_UpperChar
	RandomType_Symbol
	RandomType_NumberAndChar
	RandomType_CharAndSymbol
)

type NumberType uint8

const (
	UInt NumberType = iota
	Int
	UFloat
	Float
	Positive
	Negative
	All
)

type CmdOption struct {
	Command string
	Dir     string
}

type NumberInterface interface {
	~int | ~int8 | ~int16 | ~int32 | ~int64 | ~uint | ~uint8 | ~uint16 | ~uint32 | ~uint64 | ~float32 | ~float64
}
