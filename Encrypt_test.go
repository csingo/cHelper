package cHelper

import (
	"log"
	"testing"
)

func TestAESDecrypt(t *testing.T) {
	key := "$2a$10$j7bqvN20dx8hn/2prNiiU.6hFiiIsWLxSaQ7vX.dDYlfU2c0Wx.PW"
	encrypt := `Wpyo0usWgkzyBF6tGVX77LSq5IytcbunxHQ6pe7DP9NSXWk/z4lPbW9/Ys52YO3ZQ+VaISzwOrAWi3VhNGbvcAPDXbqHCpTWI1VNRBeSmTITyTXBNBBUddNLqDohs5lROHC5XnF46xjMB9cXyjRPLfd9KbK7URTET3TN/x3w/NHWAaIu8s0ds7tPtV+DXOB19bZVv5YubRN7GNKJ8zDDbWttpdPotM1K7vFLnr9aCbZggq4mMtq7GfKekgCtCVbaSW1oPhl8NWlsjG57y/rgFOo7+kw1DSHgGHeX/dbPqkrE+vAgQbCjJZYRNqwczvAB`
	result, err := AESDecrypt(encrypt, key)
	log.Println(result, err)
}

func TestEncrypt(t *testing.T) {
	result, err := AESEncrypt("123aaaaaaaaaaaaaaa456", Md5([]byte("123456")))
	log.Println(result, err)

}

func TestDecrypt(t *testing.T) {
	key := "$2a$10$j7bqvN20dx8hn/2prNiiU.6hFiiIsWLxSaQ7vX.dDYlfU2c0Wx.PW"
	encrypt := `Wpyo0usWgkzyBF6tGVX77LSq5IytcbunxHQ6pe7DP9NSXWk/z4lPbW9/Ys52YO3ZQ+VaISzwOrAWi3VhNGbvcAPDXbqHCpTWI1VNRBeSmTITyTXBNBBUddNLqDohs5lROHC5XnF46xjMB9cXyjRPLfd9KbK7URTET3TN/x3w/NHWAaIu8s0ds7tPtV+DXOB19bZVv5YubRN7GNKJ8zDDbWttpdPotM1K7vFLnr9aCbZggq4mMtq7GfKekgCtCVbaSW1oPhl8NWlsjG57y/rgFOo7+kw1DSHgGHeX/dbPqkrE+vAgQbCjJZYRNqwczvAB`
	result, err := AESDecrypt(encrypt, key)
	// result, err := AESDecrypt("", "")
	log.Println(result, err)
}
