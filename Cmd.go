package cHelper

import (
	"os/exec"
	"path/filepath"
	"strings"
)

func Cmd(c CmdOption) (result []byte, err error) {
	commands := strings.Split(c.Command, " ")
	if len(commands) == 0 {
		return
	}
	command := commands[0]
	params := []string{}
	if len(commands) > 1 {
		params = commands[1:]
	}

	cmd := exec.Command(command, params...)
	cmd.Dir = filepath.Clean(c.Dir)
	result, err = cmd.Output()
	return
}
