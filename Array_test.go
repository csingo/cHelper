package cHelper

import (
	"log"
	"testing"
)

func TestKeyBy(t *testing.T) {
	a := []map[string]string{
		{
			"location": "3:4",
			"age":      "3",
		},
		{
			"location": "2:3",
			"age":      "2",
		},
		{
			"location": "1:2",
			"age":      "1",
		},
		{
			"location": "5:6",
			"age":      "5",
		},
		{
			"location": "4:5",
			"age":      "4",
		},
	}
	b := GroupBy(a, func(item map[string]string) (string, bool) {
		return item["age"], true
	})
	log.Printf("%v", b)
}
