package cHelper

import (
	"fmt"
	"runtime"
	"strings"
)

func CallerFuncName() string {
	var name string
	pc, _, _, _ := runtime.Caller(1)
	full := runtime.FuncForPC(pc).Name()
	names := strings.Split(full, ".")

	if len(names) > 0 {
		name = names[len(names)-1]
	}

	return name
}

func Recover() func() {
	return func() {
		if r := recover(); r != nil {
			const size = 64 << 10
			buf := make([]byte, size)
			buf = buf[:runtime.Stack(buf, false)]
			err, ok := r.(error)
			if !ok {
				err = fmt.Errorf("%v", r)
			}
			fmt.Println(err, "panic", "stack", "\n", string(buf))
		}
	}
}
