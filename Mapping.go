package cHelper

func CopyStringMappingArray(data []map[string]string) (result []map[string]string) {
	if data == nil {
		return
	}
	for _, datum := range data {
		resultItem := make(map[string]string)
		for k, v := range datum {
			resultItem[k] = v
		}
		result = append(result, resultItem)
	}
	return
}

func CopyStringMapping(data map[string]string) (result map[string]string) {
	if data == nil {
		return
	}

	result = make(map[string]string)
	for k, v := range data {
		result[k] = v
	}

	return
}

func AlignStringMappingArrayFields(data []map[string]string) []map[string]string {
	fields := []string{}
	for _, item := range data {
		for k := range item {
			fields = append(fields, k)
		}
	}

	for index, item := range data {
		for _, field := range fields {
			if _, ok := item[field]; !ok {
				data[index][field] = ""
			}
		}
	}

	return data
}
