package cHelper

import (
	"math"
	"math/rand"
)

// RandomInt 生成随机整数
func RandomInt(min, max int) int {
	if min > max {
		min = 0
	}

	diff := max - min
	num := rand.Intn(diff)

	return min + num
}

// EmbeddingCos 使用cosine计算向量相似度
func EmbeddingCos(vector1, vector2 []float64) float64 {
	var (
		aLen  = len(vector1)
		bLen  = len(vector2)
		s     = 0.0
		sa    = 0.0
		sb    = 0.0
		count int
	)
	if aLen > bLen {
		count = aLen
	} else {
		count = bLen
	}
	for i := 0; i < count; i++ {
		if i >= bLen {
			sa += math.Pow(vector1[i], 2)
			continue
		}
		if i >= aLen {
			sb += math.Pow(vector2[i], 2)
			continue
		}
		s += vector1[i] * vector2[i]
		sa += math.Pow(vector1[i], 2)
		sb += math.Pow(vector2[i], 2)
	}
	return s / (math.Sqrt(sa) * math.Sqrt(sb))
}
