package cHelper

func ChannelAsyncPush[T any](ch chan T, data T) {
	select {
	case ch <- data:
		return
	default:
		return
	}
}
